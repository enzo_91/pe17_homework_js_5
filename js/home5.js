function createNewUser() {

    const userName = prompt('Enter your name:');
    const userLastName = prompt('Enter your last name:');
    const userAge = prompt('Enter your age, dd.mm.yyyy')


    const newUser = {
        firstName: userName,
        lastName: userLastName,
        birthday: userAge,
        getLogin() {
            return (userName[0] + userLastName).toLowerCase();
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6,10);
        },
        getAge() {
            const nowDate = new Date();
            const year = this.birthday.slice(6);
            const month = this.birthday.slice(3,5) - 1;
            const day = this.birthday.slice(0,2);
            let age = nowDate.getFullYear() - year;
            if (month > nowDate.getMonth() || (month === nowDate.getMonth() && day > nowDate.getDate())) {
                age--;
            }

            return age;
        }
    };

    return newUser;
}

const userAgeResult = createNewUser().getAge();

console.log(createNewUser().getPassword());
console.log(`Your age is ${userAgeResult}`);
